package base;

import org.openqa.selenium.WebDriver;

import utils.BrowserFactory;
import utils.ReadPropertiesFile;

public class Base {
	
	public static WebDriver driver;
	public static ReadPropertiesFile prop;
	
	public static void initialize(){
		prop = new ReadPropertiesFile();
		driver = new BrowserFactory().InitializeDriver(prop.GetDataFromConfig("browser"));
	}
	

	public static void tearDownMethod(){
		driver.close();
	}
}

package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import base.Base;

public class LoginPage extends Base{
	
	String[] emailValidation = {"emailid","emailid@gmail","emailid@gmail.com"};
	String passwordValidation = "password";

	@FindBy(xpath="//span[@class='navigation_page']")
	WebElement authentication;
	
	@FindBy(id="email")
	WebElement email;
	
	@FindBy(id="passwd")
	WebElement password;
	
	@FindBy(id="SubmitLogin")
	WebElement signInBtn;
	
	@FindBy(className="lost_password form-group")
	WebElement forgotPassword;
	
	@FindBy(xpath="//div[@class='alert alert-danger']//li")
	WebElement errorMsg;
	
//	@FindBy(className="login")
//	WebElement homeSignInBtn;
	
	public LoginPage(){
		PageFactory.initElements(driver, this);
	}
	
	public boolean isAuthenticationScreen(){
		return authentication.isDisplayed();
	}
	
//	public void clickOnSignInButton(){
//		homeSignInBtn.click();
//	}
	
	public void clickInEmail(){
		email.click();
		email.clear();
	}
	
	public void enterEmail(String em){
		email.sendKeys(em);
	}
	
	public void clickInPassword(){
		password.click();
		password.clear();
	}
	
	public void enterPassword(String pw){
		password.sendKeys(pw);
	}
	
	public void clickSignInButton(){
		signInBtn.click();
	}
	
	public String getErrorMessage(){
		return errorMsg.getText();
	}
	
	public void clickForgotPassword(){
		forgotPassword.click();
	}
	
	public String noEmailPassword(){
		clickInEmail();
		clickInPassword();
		clickSignInButton();
		
		return getErrorMessage();
	}
	
	public String wrongEmail1EmptyPassword(){
		clickInEmail();
		enterEmail(emailValidation[0]);
		clickInPassword();
		enterPassword("");
		clickSignInButton();
		
		return getErrorMessage();
	}
	
	public String wrongEmail2EmptyPassword(){
		clickInEmail();
		enterEmail(emailValidation[1]);
		clickInPassword();
		enterPassword("");
		clickSignInButton();
		
		return getErrorMessage();
	}
	
	public String correctEmailEmptyPassword(){
		clickInEmail();
		enterEmail(emailValidation[2]);
		clickInPassword();
		enterPassword("");
		clickSignInButton();
		
		return getErrorMessage();
	}
	
	public MyAccountPage enterUsernamePassword(String userName, String password){
		clickInEmail();
		enterEmail(userName);
		clickInPassword();
		enterPassword(password);
		clickSignInButton();
		
		return new MyAccountPage();
	}
//	public String correctEmailWrongPassword(){
//		System.out.println(excel.GetStringDataBySheetName("login", 3, 0) +" and "+ excel.GetStringDataBySheetName("login", 3, 1));
//		clickInEmail();
//		enterEmail(emailValidation[2]);
//		clickInPassword();
//		enterPassword(passwordValidation);
//		clickSignInButton();
//		
//		return getErrorMessage();
//	}
	
//	public String correctEmailCorrectPassword(){
//		System.out.println(excel.GetStringDataBySheetName("login", 4, 0) +" and "+ excel.GetStringDataBySheetName("login", 4, 1));
//		clickInEmail();
//		enterEmail(excel.GetStringDataBySheetName("login", 4, 0));
//		clickInPassword();
//		enterPassword(excel.GetStringDataBySheetName("login", 4, 1));
//		clickSignInButton();
//		
//		return getErrorMessage();
//	}
}

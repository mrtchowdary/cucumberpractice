package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import base.Base;

public class MyAccountPage extends Base{
	
	public MyAccountPage(){
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//h1[@class='page-heading']")
	WebElement myAccount;
	
	@FindBy(className="logout")
	WebElement logout;
	
	public String headingText(){
		return myAccount.getText();
	}
	
	public LoginPage logout(){
		logout.click();
		return new LoginPage();
	}
}

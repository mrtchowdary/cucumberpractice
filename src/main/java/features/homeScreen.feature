Feature: Automation practice validation

Scenario: Home page validation

Given user in home screen
When verify page title
Then validate screen logo
Then verify call us
Then check menu items
Then check for search suggestions
Then click on Signin button
And check if user in authentication screen
And enter credientials "mrtchowdary@gmail.com" and "wtg@123"
And check if login successful
Then click on logout
And verify if authentication screen shown
Then close browser
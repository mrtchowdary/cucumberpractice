package stepDefinitions;

import org.junit.Assert;

import pages.HomePage;
import pages.LoginPage;
import pages.MyAccountPage;
import base.Base;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VallidateHomeScrrenAndLoginStepDefinitions extends Base{
		
	HomePage homePage;
	LoginPage loginPage;
	MyAccountPage myAccountPage;
		
	@Given("^user in home screen$")
	public void user_in_home_screen(){
		initialize();
	}

	@When("^verify page title$")
	public void verify_page_title(){
		homePage = new HomePage();
		Assert.assertEquals("Home screen title is not as expected","My Store",homePage.getScreenTitle());
	}

	@Then("^validate screen logo$")
	public void validate_screen_logo(){
		Assert.assertEquals("Home page logo not diaplayed",true,homePage.isLogoDisplayed());
	}

	@Then("^verify call us$")
	public void verify_call_us(){
		Assert.assertEquals("Call us now is not as expected",true,homePage.getCallUs().contains("Call us now: 0123-456-789"));
	}

	@Then("^check menu items$")
	public void check_menu_items(){
		homePage.moveToTopMenu();
		Assert.assertEquals("Sub menu item not displayed",true,homePage.checkMenuItems());
	}

	@Then("^check for search suggestions$")
	public void check_for_search_suggestions(){
		Assert.assertEquals("Search results not found",true,homePage.searchSuggestions());
	}
	
	@Then("^click on Signin button$")
	public void click_on_Signin_button(){
		loginPage = homePage.clickOnSignInButton();
	}

	@Then("^check if user in authentication screen$")
	public void check_if_user_in_authentication_screen(){
		Assert.assertTrue("Not in authentication page", loginPage.isAuthenticationScreen());
	}

	@Then("^enter credientials \"([^\"]*)\" and \"([^\"]*)\"$")
	public void enter_credientials_and(String userName, String password){
		myAccountPage = loginPage.enterUsernamePassword(userName, password);
	}

	@Then("^check if login successful$")
	public void check_if_login_successful(){
		Assert.assertEquals("Not in Myaccount page", "MY ACCOUNT", myAccountPage.headingText());
	}

	@Then("^click on logout$")
	public void click_on_logout(){
		loginPage = myAccountPage.logout();
	}

	@Then("^verify if authentication screen shown$")
	public void verify_if_authentication_screen_shown(){
		Assert.assertTrue("On logout user not navigated to authentication page", loginPage.isAuthenticationScreen());
	}
	
	@Then("^close browser$")
	public void close_browser(){
		tearDownMethod();
	}
}

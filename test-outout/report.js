$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("./src/main/java/features/homeScreen.feature");
formatter.feature({
  "line": 1,
  "name": "Automation practice validation",
  "description": "",
  "id": "automation-practice-validation",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Home page validation",
  "description": "",
  "id": "automation-practice-validation;home-page-validation",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "user in home screen",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "verify page title",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "validate screen logo",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "verify call us",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "check menu items",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "check for search suggestions",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "click on Signin button",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "check if user in authentication screen",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "enter credientials \"mrtchowdary@gmail.com\" and \"wtg@123\"",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "check if login successful",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "click on logout",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "verify if authentication screen shown",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "close browser",
  "keyword": "Then "
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.user_in_home_screen()"
});
formatter.result({
  "duration": 30301041600,
  "status": "passed"
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.verify_page_title()"
});
formatter.result({
  "duration": 339325000,
  "status": "passed"
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.validate_screen_logo()"
});
formatter.result({
  "duration": 525033100,
  "status": "passed"
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.verify_call_us()"
});
formatter.result({
  "duration": 478496500,
  "status": "passed"
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.check_menu_items()"
});
formatter.result({
  "duration": 575341300,
  "status": "passed"
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.check_for_search_suggestions()"
});
formatter.result({
  "duration": 3051825500,
  "status": "passed"
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.click_on_Signin_button()"
});
formatter.result({
  "duration": 3848587800,
  "status": "passed"
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.check_if_user_in_authentication_screen()"
});
formatter.result({
  "duration": 210031100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "mrtchowdary@gmail.com",
      "offset": 20
    },
    {
      "val": "wtg@123",
      "offset": 48
    }
  ],
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.enter_credientials_and(String,String)"
});
formatter.result({
  "duration": 5622403500,
  "status": "passed"
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.check_if_login_successful()"
});
formatter.result({
  "duration": 145772800,
  "status": "passed"
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.click_on_logout()"
});
formatter.result({
  "duration": 2867411600,
  "status": "passed"
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.verify_if_authentication_screen_shown()"
});
formatter.result({
  "duration": 97322200,
  "status": "passed"
});
formatter.match({
  "location": "VallidateHomeScrrenAndLoginStepDefinitions.close_browser()"
});
formatter.result({
  "duration": 374952100,
  "status": "passed"
});
});